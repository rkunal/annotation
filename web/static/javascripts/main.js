// namespace
window.semantic = {
  handler: {}
};

// Allow for console.log to not break IE
if (typeof window.console == "undefined" || typeof window.console.log == "undefined") {
  window.console = {
    log  : function() {},
    info : function(){},
    warn : function(){}
  };
}
if(typeof window.console.group == 'undefined' || typeof window.console.groupEnd == 'undefined' || typeof window.console.groupCollapsed == 'undefined') {
  window.console.group = function(){};
  window.console.groupEnd = function(){};
  window.console.groupCollapsed = function(){};
}
if(typeof window.console.markTimeline == 'undefined') {
  window.console.markTimeline = function(){};
}
window.console.clear = function(){};

// ready event
semantic.ready = function() {
    var
        $document            = $(document),
        $menu                = $('#toc'),
        $hideMenu            = $('#toc .hide.item'),
        $sidebarButton       = $('.launch.icon.item'),
        handler
    ;

    // event handlers
    handler = {
    };
    semantic.handler = handler;
    // main sidebar
    $menu
        .sidebar({
            dimPage          : true,
            transition       : 'overlay',
            mobileTransition : 'uncover'
        })
    ;
    // launch buttons
    $menu.sidebar('attach events', '.launch.icon.item, .launch.button, .view-ui, .launch-item');
    $('#top-nav-search').keydown(function(event) {
        if (event.keyCode == 13) {
          if($('#top-nav-search').val() != ''){
            var slug = $('#top-nav-search').val().toString().toLowerCase().replace(/\s+/g, '-').replace(/[^\w\-]+/g, '').replace(/\-\-+/g, '-').replace(/^-+/, '') .replace(/-+$/, '');
            window.location.href = "/search/"+ slug +"/";
          }
          else {
            window.location.href = "/search/";
          }
         }
    });

}
$(document)
  .ready(semantic.ready)
;

