# coding: utf-8

from django.shortcuts import render
from django.http import HttpResponse
from .models import *
from django.http import Http404

import json
import os

from lxml import etree
import requests
from django.core.exceptions import ObjectDoesNotExist
from cobalt.render import HTMLRenderer as CobaltHTMLRenderer
from django.core.exceptions import PermissionDenied

import logging
logger = logging.getLogger(__name__)

def smart_truncate(content, length=100, suffix=' ...'):
    if len(content) <= length:
        return content
    else:
        return ' '.join(content[:length+1].split(' ')[0:-1]) + suffix
        
class CobaltDoc(object):
    pass

def edit_explain(request, document_id, section_id):
    if not request.user.is_authenticated or not request.user.is_staff:
        raise PermissionDenied
    
    editor = request.POST.get('editor1',None)
    webDoc = None
    try:
        webDoc = WebDoc.objects.get(document_id=document_id)
    except WebDoc.DoesNotExist:
        raise Http404("Not Found")
        
    if editor is not None:
        try:
            docExplainerBlock = DocExplainerBlock.objects.get(webdoc=webDoc, section_id=section_id)
            docExplainerBlock.text = editor
            docExplainerBlock.save()
        except DocExplainerBlock.DoesNotExist:
            docExplainerBlock = DocExplainerBlock(webdoc = webDoc, section_id = section_id, text = editor )
            docExplainerBlock.save()
        
    explain_html = '';
    try:
        docExplainerBlock = DocExplainerBlock.objects.get(webdoc=webDoc, section_id=section_id)
        explain_html = docExplainerBlock.text
        #@TODO: Check for multiple ones
    except DocExplainerBlock.DoesNotExist:
        pass
    
    return render(request, 'web/ldp/explain.html', {'explain_html':explain_html,'document_id':webDoc.document_id,'section_id':section_id, 'webDoc':webDoc })


#Law Display Page - 3 Column Layout
def ldp(request,doc_id=None,doc_title=None):
    
    layout = 2
    
    if doc_id is not None:
        try:
            webDoc = WebDoc.objects.get(pk=doc_id, is_deleted=False)
            document_id = webDoc.document_id
        except ObjectDoesNotExist:
            raise Http404("Document does not exist")
    else:
        raise Http404("Document does not exist")
    
    cobaltDoc = CobaltDoc()

    try:
        webDocXmlData = WebDocXmlData.objects.get(webdoc=webDoc)
    except WebDocXmlData.DoesNotExist:
        raise Http404("Document does not exist")
        
    toc_list = json.loads(webDocXmlData.toc_json)['toc']
    
    nav_toc = []
    for toc in toc_list:
        if toc.get('id', None) is not None:
            
            t = {
                'title': smart_truncate(toc['title'].replace(u'.—', ''),75),
                'hash' : toc['id']
                }
            children = toc.get('children', None )
            
            if children is not None and len(children):
                t['children'] = []
                for c in children:
                    tc = {
                        'title' : smart_truncate(c['title'].replace(u'.—', ''),75),
                        'hash' : c['id']
                    }
                    t['children'].append(tc)
            
            nav_toc.append(t)
                
        elif toc.get('type',None) is not None and toc.get('type',None) == 'doc':
            t = {
                'title' : smart_truncate(toc['title'].replace(u'.—', ''),75),
                'hash'  : 'component-'+toc['title'].replace(" ","").lower()
            }
            nav_toc.append(t)

    cobaltDoc.id = webDoc.document_id
    cobaltDoc.frbr_uri = webDoc.frbr_url
    cobaltDoc.title = webDoc.short_title
    cobaltDoc.country = 'in'
    cobaltDoc.language = 'en'
    cobaltDoc.draft = False
    cobaltDoc.expression_date = webDoc.date

    cobaltDoc.document_xml = webDocXmlData.xml_data
    
    metaData = {}
    metaData['short_title'] = webDoc.short_title
    metaData['locality'] = webDoc.locality
    metaData['category'] = webDoc.category

    actData = {}

    root = etree.fromstring(cobaltDoc.document_xml)
    
    nsmap = {}
    #Hack as etree does not allow empty namespace . Need to read more to understand this
    nsmap['ny'] = root.nsmap[None]
    
    
    html_cleanup = '''<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
                "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" [
                <!ENTITY nbsp ' '>
                <!ENTITY lsquo "'">
                <!ENTITY rsquo "'">
                <!ENTITY ndash "-">
                <!ENTITY mdash "-">
                <!ENTITY ldquo '"'>
                <!ENTITY rdquo '"'>
                <!ENTITY shy '-'>
                <!ENTITY hellip "...">
                
                
                ]>'''
                
    try:
        explainers  = DocExplainerBlock.objects.filter(webdoc=webDoc).exclude(section_id='intro').order_by('id')
        for explain in explainers:
            layout = 3
            e = root.find('.//ny:section[@id="'+explain.section_id+'"]', nsmap)
            if e is not None:
                explain.text = explain.text.replace('<p></p>', '<enewline></enewline>')
                explain.text = explain.text.replace('<br/>', '<enewbr></enewbr>')
                
                xml_text = '<explain><div id="explainer-'+ str(explain.id) +'">'+explain.text.replace('\r\n', '')+'</div></explain>'
        
                x = etree.fromstring(html_cleanup + xml_text)
                e.append(x)
            
            e = root.find('.//ny:chapter[@id="'+explain.section_id+'"]', nsmap)
            if e is not None:
                explain.text = explain.text.replace('<p></p>', '<enewline></enewline>')
                explain.text = explain.text.replace('<br/>', '<enewbr></enewbr>')
                
                xml_text = '<explain><div id="explainer-'+ str(explain.id) +'">'+explain.text.replace('\r\n', '')+'</div></explain>'
        
                x = etree.fromstring(html_cleanup + xml_text)
                e.set('explained-chapter','1')
                e.append(x)
                
    except ObjectDoesNotExist:
        pass
        
    if request.user.is_authenticated and request.user.is_staff:

        for section in  root.findall(".//ny:section", nsmap):
            num = section.find('./ny:num',nsmap)
            num.set('explainurl','/law/explain/'+str(document_id)+'/section-'+num.text.split(".")[0])
        
        for chapter in  root.findall(".//ny:chapter", nsmap):
            num = chapter.find('./ny:num',nsmap)
            num.set('explainurl','/law/explain/'+str(document_id)+'/chapter-'+num.text.split(".")[0])
    else:
        for section in  root.findall(".//ny:section", nsmap):
            num = section.find('./ny:num',nsmap)
            num.set('explainurl','#section-'+num.text.split(".")[0])

        for chapter in  root.findall(".//ny:chapter", nsmap):
            num = chapter.find('./ny:num',nsmap)
            num.set('explainurl', '#chapter-'+num.text.split(".")[0])

    if layout == 3:

        xsl_path = os.path.abspath(os.path.join(os.path.dirname(__file__)+'/explained_act.xsl'))
    else:
        xsl_path = os.path.abspath(os.path.join(os.path.dirname(__file__)+'/plain_act.xsl'))
        
    cobaltDoc.document_xml = etree.tostring(root)

    cobalt_kwargs = {}
    cdoc = CobaltHTMLRenderer(act=cobaltDoc, xslt_filename=xsl_path, **cobalt_kwargs)
    cdoc_html = cdoc.render_xml(cobaltDoc.document_xml)
    
    intro = None
    intro_html = ''
    intro_image = ''
    intro_title = ''
    
    intro = None
    try:
        intro  = DocExplainerBlock.objects.get(webdoc=webDoc,section_id='intro')
    except DocExplainerBlock.DoesNotExist:       
        pass

    return render(request, 'web/ldp/index.html', {'docIntroBlock':intro,'cdoc_html':cdoc_html,'metaData':metaData, 'actData': actData,  'document_id' : cobaltDoc.id,'nav_toc':nav_toc, 'layout':layout, 'webDoc': webDoc, 'docExplainerBlock__intro': intro})
    