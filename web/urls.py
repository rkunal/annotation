from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^law/explain/(?P<document_id>[0-9]+)/(?P<section_id>[\-a-z0-9A-Z]+)/$', views.edit_explain, name='edit_explain'),
    url(r'^law/(?P<doc_id>[0-9]+)/(?P<doc_title>[\w-]+)/$', views.ldp, name='ldp'),
]