from django.contrib import admin
from .models import *


class WebDocAdmin(admin.ModelAdmin):
    list_display = ('document_id', 'short_title', 'category','locality')
    search_fields = ('document_id','short_title','locality',)
    list_filter = ('locality_id',)

class WebDocXMLAdmin(admin.ModelAdmin):
    list_display = ('webdoc',)
    search_fields = ('webdoc__id',)
    list_filter = ('webdoc',)
        
class DocExplainerBlockAdmin(admin.ModelAdmin):
    pass
    
admin.site.register(WebDoc, WebDocAdmin)
admin.site.register(WebDocXmlData, WebDocXMLAdmin)
admin.site.register(DocExplainerBlock, DocExplainerBlockAdmin)